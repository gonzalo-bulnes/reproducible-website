---
layout: event_detail
title: Agenda
event: berlin2016
order: 10
permalink: /events/berlin2016/agenda/
---


Day 1
-----

*Tuesday, December 13*

*    09.15 Breakfast
*    10.00 Opening Session
*    11.00 Project Showcase
     -   **Reproducible FreeBSD**
     -   **Status stretch and buster**
     -   **buildinfo.debian.net**
     -   **openSUSE**
     -   **Test reproducible-builds.org. How we constantly test Debian**
     -   **Eliminating absolute build paths from debusgging info and other things**
     -   **OpenWrt, coreboot, LEDE**
     -   **F-Droid. Reproducible Adroid apps**
     -   **Building reproducible Tails ISO images [work in progress]**
*    11.50 Break
*    12.15 **[Agenda Brainstorming]({{ "/events/berlin2016/agendabrainstorming/" | relative_url }})**

*    13.20 Lunch Break
*    14.30 Working Sessions I
     -   **[diffoscope]({{ "/events/berlin2016/diffoscope/" | relative_url }})**
     -   **[reprotest]({{ "/events/berlin2016/reprotest/" | relative_url }})**
     -   **[Documentation I]({{ "/events/berlin2016/documentation/" | relative_url }})**
     -   **[User verification]({{ "/events/berlin2016/userverification/" | relative_url }})**
     -   **[Embedded / Coreboot]({{ "/events/berlin2016/embedded/" | relative_url }})**
     -   **[RPM]({{ "/events/berlin2016/RPM/" | relative_url }})**

*    16.00 Closing Session

*    Proposals for hacking sessions to take place later in the afternoon:
     -   **SquashFS**
     -   **FreeBSD filesystems**
     -   **Python packages in git**
     -   **Gettext**
     -   **Make diffoscope deal with Android apks**
     -   **Markdowns**

*    16:20 Adjourn
*    16:30 Hacking
     -   **[http://pkgsrc.se/files.php?messageId=20161213154459.CB044FBA6@cvs.NetBSD.org](http://pkgsrc.se/files.php?messageId=20161213154459.CB044FBA6@cvs.NetBSD.org)**

Day 2
-----

*Wednesday, December 14*

*    09.15 Breakfast
*    10.00 Opening Session
*    10:20 Working Sessions II

     -   **[.buildinfo files]({{ "/events/berlin2016/buildinfofiles/" | relative_url }})**
     -   **[RPM II]({{ "/events/berlin2016/RPMII/" | relative_url }})**
     -   **[Reproducible images]({{ "/events/berlin2016/images/" | relative_url }})**
     -   **[Defining Reproducible Builds I]({{ "/events/berlin2016/reproduciblebuildsdefinition/" | relative_url }})**
     -   **[User policies]({{ "/events/berlin2016/userpolicies/" | relative_url }})**
     -   **[Test infrastructure]({{ "/events/berlin2016/testinfrastructure/" | relative_url }})**
     -   **[Gettext]({{ "/events/berlin2016/gettext/" | relative_url }})**

*    11.45 Break
*    12.30 Skill Share

     Participants are invited to choose a skill they'd like to share.
     Focused 1-3 person 30-minute conversations

     -   **git-based packaging**
     -   **How to use C sanitizers + fuzzing**
     -   **How to make storage deduplicate and incentivize reproducible builds hash**
     -   **How to use buildinfos to analyze/test reproducibility**
     -   **Fedora AMA**
     -   **How to use emacs**
     -   **How to run a start-up**
     -   **How to apply for funding from CII**
     -   **How to improve cross-distro packaging [https://maintainer.zq1.de](https://maintainer.zq1.de)**
     -   **How (not) to use iframes on awesome webpages *reproducible-bla.org**
     -   **How to sign code in git (and correctly verify the signatures)**
     -   **Ask me anything about building on OSX**
     -   **How to do automatic hardware testing**

*    13.00 Lunch Break
*    14.20 Working Sessions III

     -   **[What else for the Auditable Ecosystem?]({{ "/events/berlin2016/whatelse/" | relative_url }})**
     -   **[What else]({{ "/events/berlin2016/whatelse/" | relative_url }})** instead of **[Binary Transparency]({{ "/events/berlin2016/binarytransparency/" | relative_url }})**
     -   **[SOURCE_PREFIX_MAP]({{ "/events/berlin2016/SOURCE_PREFIX_MAP/" | relative_url }})**
     -   **[Documentation II]({{ "/events/berlin2016/documentationII/" | relative_url }})**
     -   **[Defining Reproducible Builds definition II]({{ "/events/berlin2016/reproduciblebuildsdefinitionII/" | relative_url }})**
     -   **[Bootstrapping I]({{ "/events/berlin2016/bootstrapping/" | relative_url }})**
     -   **[Use cases]({{ "/events/berlin2016/usecases/" | relative_url }})**
     -   **[GPL compliance]({{ "/events/berlin2016/gpl-compliance/" | relative_url }})**

*    16.00 Closing Session

* Proposals for hacking sessions to take place later in the afternoon:
     -   **[Secure GIT]({{ "/events/berlin2016/securegit/" | relative_url }})**
     -   **Make build images reproducible**
     -   **diffoscope**
     -   **Documentation**
     -   **Funding and CII**
     -   **RPM and hacking**
     -   **Nix build stuff to be incorporated with tests at [https://tests.reproducible-builds.org](https://tests.reproducible-builds.org)**
     -   **Bootstrap test jenkins to replicate [https://tests.reproducible-builds.org](https://tests.reproducible-builds.org)**
     -   **Embedded images cross-distro**

*    16:30 Adjourn
*    16.40 Hacking


Day 3
-----

*Thursday, December 15*

*    09.15 Breakfast
*    10.00 Opening Session
*    10:20 Working Sessions VI

     -   **[Cross-distro collaboration]({{ "/events/berlin2016/crossdistro/" | relative_url }})**
     -   **[Bootstrapping II]({{ "/events/berlin2016/bootstrappingII/" | relative_url }})**
     -   **[Documentation III]({{ "/events/berlin2016/documentationIII/" | relative_url }})**
     -   **[Binary Transparency II]({{ "/events/berlin2016/binarytransparencyII/" | relative_url }})**
     -   **[State of Reproducible Builds]({{ "/events/berlin2016/stateofreproduciblebuilds/" | relative_url }})**

*    11:50 Break
*    12:05 Reporting session outcomes

Proposals for hacking sessions to take place today:

* **[Diffoscope Debugging]({{ "/events/berlin2016/diffoscopedebug/" | relative_url }})**
* **diffoscope everything**
* **RPM**
* **Documentation**
* **buildinfo**
* **looking at buildinfo coming from different architectures**
* **Gettext**
* **FreeBSD filesystems**
* **Android documentation**
* **Reproducing the test environment and documenting it**
* **bootstrapped.org**
* **Debian infrastructure**
* **Binary transparency log**
* **Android infrastructure**

*    12:30 Hacking
     -   ** F-droid and append-only publication log** (documented through [pictured sketch](/images/berlin2016/FDroidpublicationlog_01.JPG))

*    13:15 Lunch Break
*    14:30 **[2017 Look Ahead]({{ "/events/berlin2016/2017lookahead/" | relative_url }})**
*    15.00 Closing Session
*    15.30 Adjourn

